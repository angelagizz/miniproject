import Login from './components/layout/Login'

function App() {
  return (
    <div className="App">
      <Login />
    </div>
  );
}

export default App;

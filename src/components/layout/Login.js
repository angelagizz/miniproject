import { useState } from "react";
// import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import "./Login.css";
import ModalLogin from "react-modal";
import ModalRegister from "react-modal";

import { addLogin } from "../../reduxStore/actions/addLogin";

import { useHistory } from "react-router-dom";

import { register, login } from "../../services/userService";

const Login = () => {
  const [email, setEmail] = useState("");
  const [emailRg, setEmailRg] = useState("");
  const [password, setPassword] = useState("");
  const [passwordRg, setPasswordRg] = useState("");
  const [fullnameRg, setFulnameRg] = useState("");
  const [usernameRg, setUsernameRg] = useState("");
  // const allUser = useSelector(state => state.user.alluser.list);
  const [loginmod, setLoginmod] = useState(false);
  const [regmod, setRegmod] = useState(true);

  const dispatch = useDispatch();
  const history = useHistory();

  const handlerRegister = e => {
    e.preventDefault();
    // const store = window.localStorage;
    register(fullnameRg, usernameRg, emailRg, passwordRg)
      .then(response => {
        // const { username = usernameRg, password = passwordRg } = response.data;
        console.log(response.data);
        // const temp = { username, password };
        // store.setItem("user", JSON.stringify(temp));
        // dispatch(addRegister(username, password));
      })
      .catch(error => {
        console.log(error);
      });
    // dispatch(addLogin(username, password));
    // history.push("move/overview");
  };

  const handleSubmit = e => {
    e.preventDefault();
    const store = window.localStorage;
    login(email, password)
      .then(response => {
        const { email, token } = response.data;
        // console.log(response.data);
        const temp = { email, token };
        store.setItem("user", JSON.stringify(temp));
        dispatch(addLogin(email, token));
      })
      .catch(error => {
        console.log(error);
      });
      setRegmod(false)
    // dispatch(addLogin(username, password));
  };

  // const userHandler = e => {
  //   e.preventDefault();
  //   setLoginmod(true);
  // };

  return (
    <div className="pagar">
      <div>{/* <button onClick={userHandler}>Sign In</button> */}</div>
      <>
        <ModalRegister
          isOpen={regmod}
          onRequesClose={e => {
            e.preventDefault();
            setLoginmod(false);
          }}
        >
          <div className="section">
            <div className="container">
              <form className="form" onClick={handlerRegister}>
                <h3>MovReact</h3>

                <h6>Full Name</h6>
                <input
                  type="fullname"
                  onChange={e => setFulnameRg(e.target.value)}
                  placeholder="Full Name"
                  value={fullnameRg}
                  // name="name"
                ></input>

                <h6>Email</h6>
                <input
                  type="text"
                  // name="email"
                  value={emailRg}
                  placeholder="Your email here"
                  onChange={e => setEmailRg(e.target.value)}
                ></input>

                <h6>Password</h6>
                <input
                  type="password"
                  // name="password"
                  value={passwordRg}
                  placeholder="Password"
                  onChange={e => setPasswordRg(e.target.value)}
                ></input>

                <button
                  className="btn-signupfa"
                  onClick={handlerRegister}
                >
                  Sign Up
                </button>

                <p>
                  Already have an account?
                  <button
                    className="btn-signinfa"
                    onClick={() => {
                      setLoginmod(true);
                      setRegmod(false);
                    }}
                  >
                    Log In
                  </button>
                </p>
                {/*  */}
              </form>
            </div>
          </div>
        </ModalRegister>
      </>
      <>
        <ModalLogin
          isOpen={loginmod}
          onRequestClose={e => {
            e.preventDefault();
            setRegmod(false);
          }}
        >
          <div className="section">
            <div className="container">
              <form className="form" onSubmit={handleSubmit}>
                <h3>MovReact</h3>
                <h6>Email</h6>
                <input
                  type="username"
                  onChange={e => setEmail(e.target.value)}
                  placeholder="Username"
                  value={email}
                  // name="name"
                ></input>

                <h6>Password</h6>
                <input
                  type="password"
                  // name="password"
                  value={password}
                  placeholder="Password"
                  onChange={e => setPassword(e.target.value)}
                ></input>

                <button className="btn-signinfb">Log in</button>
              </form>
            </div>
          </div>
        </ModalLogin>
      </>
    </div>
  );
};

export default Login;

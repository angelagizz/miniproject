import React from "react";
import { useHistory } from "react-router-dom";
// import "./Homepage.css";
// import Modallogin from "react-modal";

export const Homepage = () => {
  const history = useHistory();
  const handleSubmit = () => {
    history.push("/login/addlogin");
  };
  return (
    <div>
      <div className="header">
        <form className="list" onSubmit={handleSubmit}>
          <h1>MovReact</h1>
          <ul>
            <li>
              <button className="btn-signin">Sign In</button>
            </li>
            <li>
              <input type="" placeholder="Search Movie"></input>
            </li>
          </ul>
        </form>
      </div>
    </div>
  )}

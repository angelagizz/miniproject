export const addRegister = (fullname, password, username) => {
  return {
    type: "register/addRegister",
    payload: {
      fullname,
      password,
      username,
    },
  };
};

export const addLogin = (email, password) => {
  return {
    type: "login/addlogin",
    payload: {
      email,
      password,
    },
  };
};

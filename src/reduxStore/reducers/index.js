import { createStore, combineReducers } from 'redux';
import reviewReducer from './review';
import loginReducer from "./loginRs";
import { homepageReducer } from "./homepageReducer";
import registerReducer from "./registerRs";


const rootReducer = combineReducers ({
    review: reviewReducer,
    login: loginReducer,
    register: registerReducer,
    homepage: homepageReducer,

})

const store = createStore(rootReducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;
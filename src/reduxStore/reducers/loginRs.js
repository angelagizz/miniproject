const initialState = {
    email: null,
    token: null,
  };
  
  const loginReducer = (state = initialState, action) => {
    switch (action.type) {
      case "login/addlogin":
        return {
          ...state,
          email: action.payload.email,
          token: action.payload.token,
        };
      default:
        return state;
    }
  };
  
  export default loginReducer;
  
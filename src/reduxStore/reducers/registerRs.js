const initialState = {
    fullname: null,
    password: null,
    username: null,
  };
  
  const registerReducer = (state = initialState, action) => {
    switch (action.type) {
      case "login/addlogin":
        return {
          ...state,
          fullname: action.payload.fullname,
          password: action.password.password,
          username: action.payload.username,
        };
      default:
        return state;
    }
  };
  
  export default registerReducer;
  